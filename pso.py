# Imports for the libraries necessary
import random as rand
from math import sin, cos
from sys import argv
from os import _exit


# Class that makes a single particle
class Particle:
    # Default constructor
    def __init__(self):
        self.pos = [rand.uniform(-5, 5), rand.uniform(-5, 5)]
        self.vel = [0, 0]
        self.fitness = calculate_fitness(self.pos[0], self.pos[1])
        self.bestPos = [self.pos[0], self.pos[1]]


# Function to calculate the fitness of a particle
def calculate_fitness(x, y):
    return float(sin(2 / x) + sin(2 * x) + cos(x) + sin(20 / y))


# Function to calculate the velocity of a particle
def calculate_velocity(particle, c1, c2, gBest):
    w = 0.9
    velocity = []

    for vel, pos, best, g in zip(particle.vel, particle.pos, particle.bestPos, gBest):
        velocity.append((w * vel) + (c2 * rand.random() * (best - pos)) + (c1 * rand.random() * (g - pos)))
    return velocity


# Function to move the particle
def move(particle, dt):
    for vel, i in zip(particle.vel, range(2)):
        particle.pos[i] += vel * dt

        # If statement to check and see if the position should be 5 or -5
        if particle.pos[i] > 5:
            particle.pos[i] = 5
        elif particle.pos[i] < -5:
            particle.pos[i] = -5

    # After the particle is moved calculate it's fitness
    particle.fitness = calculate_fitness(particle.pos[0], particle.pos[1])


# Get the best fitness/particle of a swarm
def getBest(swarm, gBest):
    best = swarm[0]
    for particle in swarm:
        if particle.fitness > best.fitness:
            best = particle
    if gBest is None:
        return best.pos
    if best.fitness > calculate_fitness(gBest[0], gBest[1]):
        return best.pos
    else:
        return gBest


# Function that intializes the swarm
def initSwarm(n):
    swarm = []
    while len(swarm) < n:
        swarm.append(Particle())
    return swarm


# Main method
if __name__ == '__main__':
    n = 10      # number of particles
    c1 = 0.75   # global sticky-ness value
    c2 = 0.5    # self sticky-ness value
    dt = 0.1    # how frequently we update our position
    particle_files = [] * n

    # If statement for if the user gives args after the command
    if len(argv) > 1:
        if argv[1] in ['-help', '-h', '--help', 'help']:
            print('python pso.py globalStickyness(default = 0.75)', end=" ")
            print('selfStickyness(default = 0.5) numParticles(default = 10)')
            _exit(0)
        elif len(argv) == 4:
            c1 = float(argv[1])
            c2 = float(argv[2])
            n = int(argv[3])

    # Intiailize the swarm
    swarm = initSwarm(n)

    # Get best position
    gBestPosition = getBest(swarm, None)

    # Print out every particle's position
    particle_num = 0
    for particle in swarm:
        particle_files.append(open("particle0" + str(particle_num), 'w'))
        particle_files[particle_num].write("{0}, {1}\n".format(particle.pos[0], particle.pos[1]))
        particle_num += 1

    time_step = 0  # Variable to keep track of timestep
    while time_step < 100:
        time_step += 1

        # Calaculate and move every particle in the swarm
        for particle in swarm:
            particle.vel = calculate_velocity(particle, c1, c2, gBestPosition)
            move(particle, dt)

        # Find the new best particle
        gBestPosition = getBest(swarm, gBestPosition)

        # Print out every particle's position
        particle_num = 0
        for particle in swarm:
            particle_files[particle_num] = open("particle0" + str(particle_num), 'a')
            particle_files[particle_num].write("{0}, {1}\n".format(particle.pos[0], particle.pos[1]))
            particle_num += 1

        # For loop for closing all of the particle files
        for i in range(0, n):
            particle_files[i].close()
